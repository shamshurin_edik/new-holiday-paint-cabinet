import Vue from 'vue'
import VueRouter from 'vue-router'
import "./style/main.scss"
import axios from 'axios'
import config from './libs/config.js'
import cookie from './libs/cookie.js'
const myApi = axios.create(config.AXIOS_CONF);

const Cabinet = () => import('./Cabinet.vue');
const Signin = () => import('./Signin.vue');
const Signup = () => import('./Signup.vue');
const Forget = () => import('./Forget.vue');

Vue.use(VueRouter);

const routes = [
  { path: '/', component: Cabinet },
  { path: '/signin', component: Signin },
  { path: '/:page', component: Cabinet },
  { path: '/:page/:id', component: Cabinet },
  { path: '/forget/:secure/:id'}

];

const router = new VueRouter({
  mode: 'history',
  routes: routes
})

const app = new Vue({
  el: '#app',
  data: {
    auth: false,
    user: {
      id: null,
      level: null,
      secure: null
    }
  },
  router,
  methods: {
    authentication: function () {   
      var home = false;
      if (this.$route.params.secure && this.$route.params.id) {
        var secure = this.$route.params.secure;
        var id = this.$route.params.id;
        cookie.setCoocie('id', id, { expires: 360000, path:'/' });
        cookie.setCoocie('secure', secure, { expires: 360000, path:'/' });        
        home = true;
      }
      
    	if (cookie.getCoocie('id') && cookie.getCoocie('secure')) {
        var data = new FormData();
        this.user.id = cookie.getCoocie('id');
        this.user.secure = cookie.getCoocie('secure');        
        data.append("data", JSON.stringify(this.user));

        myApi.post(config.PATH.users.authentication, data)
          .then(function (response) {
            if (response.data.error === 0) {
              //Ошибок нет, продливаем куки
              this.reload_user_data(response.data.data.id,response.data.data.password,response.data.data.level);
              if (home) {
                this.$router.push({ path: '/report' });
              }
            } else {
              //Произошла ошибка, отчистить куки и выйти
              cookie.deleteCoocie('id');
              cookie.deleteCoocie('secure');
              this.$router.push({ path: '/signin' });
            }
          }.bind(this))
          .catch(function (error) {
            console.log(error);
          });
      } else {        
    		this.$router.push({ path: '/signin' });
    	}
    },
    reload_user_data: function (id, secure, level) {
      this.user.id = id;
      this.user.level = level;
      this.user.secure = secure;
      cookie.setCoocie('id', id, { expires: 360000, path:'/' });
      cookie.setCoocie('secure', secure, { expires: 360000, path:'/' });
      cookie.setCoocie('level', level, { expires: 360000, path:'/' });
    }
  },
  created: function() {
    this.authentication();  
  }
});