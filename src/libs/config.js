if (process.env.NODE_ENV === 'production') {
  var url = 'http://update.holiday-paint.ru/';
  var api_url = 'http://update.holiday-paint.ru/api/';
} else {
  var url = 'http://new.holiday-paint.test/';
  var api_url = 'http://new.holiday-paint.test/';
}

const config = {
  API_URL: api_url,
  URL: url,
  PATH: {
    users: {
      path: 'users',
      auth: 'users/auth.php',
      authentication: 'users/authentication.php',
      forget: 'users/forget.php',
      profile: 'users/profile.php',
      edit: 'users/edit.php',
      reg: 'users/reg.php'
    }
  },
  AXIOS_CONF: {
    baseURL: api_url,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }
}

export default config;
