<?php
class Database {

  private $host = "localhost";
  private $db_name = "holiday-paint";
  private $username = "root";
  private $password = "666999";
  public $conn;

  public function getConnection(){

    $this->conn = null;

    try {
        $this->conn = new mysqli($this->host, $this->username, $this->password, $this->db_name);
        $this->conn->set_charset("utf8");
    } catch(mysqli_sql_exception $e) {
        echo "Connection error: " . $e;
    }

    return $this->conn;
  
  }
}

?>