<?php
require_once('../config.php');
require_once('../connect.php');

$t_users = $tables->users; // users

$fallback = function($mysqli) {
  return Action::test($mysqli); // не забываем return
};

echo Wrap::wrapper($fallback);

Class Action {
  static function test($data) {
    $mysqli = $data['mysqli'];
    $t_users = $GLOBALS['t_users'];
    
    $user = json_decode( $_POST['data'] );
    $email = $user->{'email'};

    $stmt = $mysqli->prepare("SELECT * FROM $t_users WHERE email=?");
    $stmt->bind_param("s", $email);
    $stmt->execute();
    $result = $stmt->get_result();
    $data['data'] = $result->fetch_assoc();
    $stmt->close();
    return $data;
  }
}

?>