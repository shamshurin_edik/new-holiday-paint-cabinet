<?php
require_once('../config.php');
require_once('../connect.php');

$table = $tables->users; // users

$fallback = function($mysqli) {
  return Action::test($mysqli); // не забываем return
};

echo Wrap::wrapper($fallback);

Class Action {
  static function test($data) {
    $mysqli = $data['mysqli'];
    $table = $GLOBALS['table'];

    $user = json_decode($_POST['data']);

    $email = $user->{'email'};
    $password = md5(trim($user->{'password'}));
    $invite = $user->{'invite'};


    $stmt = $mysqli->prepare("SELECT * FROM $table WHERE invite=?");
    $stmt->bind_param("s", $invite);
    $stmt->execute();
    $result = $stmt->get_result();
    $stmt->close();
    $data['data'] = $result->fetch_assoc();
    if (empty($data['data']['id']) or !empty($data['data']['email'])) {
      $data['error'] = 2;
      $data['error_text'] = 'Неверный код';
    } else {
      $stmt = $mysqli->prepare("UPDATE $table SET email=?, password=? WHERE invite=?");
      $stmt->bind_param("sss", $email, $password, $invite);
      $stmt->execute();
      $stmt->close();
      $data['data']['password'] = $password;
      $data['data']['email'] = $email;
    }
    return $data;
  }
}

?>