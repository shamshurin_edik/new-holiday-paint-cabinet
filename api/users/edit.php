<?php
require_once('../config.php');
require_once('../connect.php');

$t_info = $tables->users_info; // users
$t_users = $tables->users;

$fallback = function($mysqli) {
  return Action::test($mysqli); // не забываем return
};

echo Wrap::wrapper($fallback);

Class Action {
  static function test($data) {
    $mysqli = $data['mysqli'];
    $t_info = $GLOBALS['t_info'];
    $t_users = $GLOBALS['t_users'];
    
    $user = json_decode($_POST['data']);
    $id = $user->{'id'};
    $name = $user->{'name'};
    $s_name = $user->{'s_name'};
    $f_name = $user->{'f_name'};
    $story = $user->{'story'};
    $email = $user->{'email'};
    $phone = preg_replace("/[^0-9]/", '', $user->{'phone'});
    if (strlen($phone) > 10) $phone = substr("$phone", -10);

    $save_user = $mysqli->prepare("INSERT INTO $t_info (id, name, s_name, f_name, phone, story) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE name=?, s_name=?, f_name=?, phone=?, story=?");
    $save_user->bind_param('issssssssss', $id, $name , $s_name, $f_name, $phone, $story, $name , $s_name, $f_name, $phone, $story);
    $save_user->execute();
    if ($save_user) {
      $data['error'] = 0;
    } else $data['error'] = 2;
    $save_user->close();

    if ($user->{'secure'}) {
      if ($user->{'password'} == $user->{'d_password'}) {
        $password = md5($user->{'password'});
        $save_user = $mysqli->prepare("UPDATE $t_users SET password=?, email=? WHERE id=?");
        $save_user->bind_param("ssi", $password, $email, $id);
      }
    } else {
      $save_user = $mysqli->prepare("UPDATE $t_users SET email=? WHERE id=?");
      $save_user->bind_param("si", $email, $id);
    }
    $save_user_result = $save_user->execute();
    $save_user->close();

    return $data;
  }
}

?>