<?php
require_once('../config.php');
require_once('../connect.php');

$t_info = $tables->users_info; // users
$t_cities = $tables->cities;
$t_users = $tables->users;
$t_locations = $tables->locations;

$fallback = function($mysqli) {
  return Action::test($mysqli); // не забываем return
};

echo Wrap::wrapper($fallback);

Class Action {
  static function test($data) {
    $mysqli = $data['mysqli'];
    $t_info = $GLOBALS['t_info'];
    $t_cities = $GLOBALS['t_cities'];
    $t_users = $GLOBALS['t_users'];
    $t_locations = $GLOBALS['t_locations'];

    $user = json_decode( $_POST['data'] );
    $id = $user->{'id'};

    // $stmt = $mysqli->prepare("SELECT $t_info.*, $t_cities.name AS city, $t_users.email AS email FROM $t_info INNER JOIN $t_cities ON $t_info.city = $t_cities.id AND $t_info.id = ? INNER JOIN $t_users ON $t_info.id = $t_users.id");
    $stmt = $mysqli->prepare("SELECT $t_info.*, $t_users.email AS email, $t_cities.name AS city FROM $t_users LEFT JOIN $t_info ON $t_info.id = $t_users.id LEFT JOIN $t_cities ON $t_cities.id = $t_info.city WHERE $t_users.id = ?");
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $result = $stmt->get_result();
    $data['data'] = $result->fetch_assoc();
    $stmt->close();

    $stmt = $mysqli->prepare("SELECT * FROM $t_locations WHERE user=?");
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $result = $stmt->get_result();
    $data['data']['locations'] = array();
    while ($row = $result->fetch_assoc()) {
      $data['data']['locations'][] = $row;
    }
    $stmt->close();

    return $data;
  }
}

?>