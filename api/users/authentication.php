<?php
require_once('../config.php');
require_once('../connect.php');

$table = $tables->users; // users

$fallback = function($mysqli) {
  return Action::test($mysqli); // не забываем return
};

echo Wrap::wrapper($fallback);

Class Action {
  static function test($data) {
    $mysqli = $data['mysqli'];
    $table = $GLOBALS['table'];
    
    $user = json_decode( $_POST['data'] );
    $id = $user->{'id'};
    $secure = $user->{'secure'};

    $stmt = $mysqli->prepare("SELECT * FROM $table WHERE id=? AND password=? AND level<>0");
    $stmt->bind_param("is", $id, $secure);
    $stmt->execute();
    $result = $stmt->get_result();
    $data['data'] = $result->fetch_assoc();
    $stmt->close();
    return $data;
  }
}

?>