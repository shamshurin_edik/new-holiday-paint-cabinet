<?php
require_once('../config.php');
require_once('../connect.php');

$table = $tables->users; // users

$fallback = function($mysqli) {
  return Action::test($mysqli); // не забываем return
};

echo Wrap::wrapper($fallback);

Class Action {
  static function test($data) {
    $mysqli = $data['mysqli'];
    $table = $GLOBALS['table'];

    $user = json_decode($_POST['data']);

    $email = $user->{'email'};
    $password = md5(trim($user->{'password'}));
    $stmt = $mysqli->prepare("SELECT * FROM $table WHERE email=? AND password=? AND level<>0");
    $stmt->bind_param("ss", $email, $password);
    $stmt->execute();
    $result = $stmt->get_result();
    $data['data'] = $result->fetch_assoc();
    if (empty($data['data']['id'])) {
      $data['error'] = 2;
      $data['error_text'] = 'Пользователь не найден';
    }
    $stmt->close();
    return $data;
  }
}

?>