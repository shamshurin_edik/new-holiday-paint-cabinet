<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once './config/database.php';
include_once './config/tables.php';


function route($method, $urlData, $formData) {


	$database = new Database();
	$tables = new Tables();
	$db = $database->getConnection();
	$data = array();


	if ($method === 'GET') {
		// 
		if (count($urlData) === 0) {
			// если нет дополнительных параметров достаём всех пользоателей
			$get_user = $db->prepare("
				SELECT $tables->users_info.*, $tables->users.email AS email, $tables->cities.name AS city FROM $tables->users 
				LEFT JOIN $tables->users_info ON $tables->users_info.id = $tables->users.id 
				LEFT JOIN $tables->cities ON $tables->cities.id = $tables->users_info.city
			");
			$get_user->execute();
			$result_users = $get_user->get_result();
			$get_user->close();

			while ($row = $result_users->fetch_assoc()) {
				$get_locations = $db->prepare("SELECT * FROM locations WHERE user=?");
				$get_locations->bind_param("i", $row['id']);
				$get_locations->execute();
				$result_locations = $get_locations->get_result();
				$get_locations->close();
	
				$row['locations'] = array();
				while ($location = $result_locations->fetch_assoc()) {
					$row['locations'][] = $location;
				}
				$data[] = $row;
			}

			$db->close();

			echo json_encode($data);
			return;
		}

		if (count($urlData) === 1) {
			// если один параметр получаем id и достаём его данные
			// Получаем id пользователя
			$id = $urlData[0];

			$get_user = $db->prepare("
				SELECT $tables->users_info.*, $tables->users.email AS email, $tables->cities.name AS city FROM $tables->users 
				LEFT JOIN $tables->users_info ON $tables->users_info.id = $tables->users.id 
				LEFT JOIN $tables->cities ON $tables->cities.id = $tables->users_info.city 
				WHERE $tables->users.id = ?
			");

			$get_user->bind_param("i", $id);
			$get_user->execute();
			$result = $get_user->get_result();
			$data = $result->fetch_assoc();
			$get_user->close();

			$get_locations = $db->prepare("SELECT * FROM locations WHERE user=?");
			$get_locations->bind_param("i", $id);
			$get_locations->execute();
			$result = $get_locations->get_result();
			$get_locations->close();

			$data['locations'] = array();

			while ($row = $result->fetch_assoc()) {
				$data['locations'][] = $row;
			}

			$db->close();

			echo json_encode($data);
			return;
		}

	}

	if ($method === 'PUT') {
    
    $user = json_decode($_POST['data']);
    $id = $user->{'id'};
    $name = $user->{'name'};
    $s_name = $user->{'s_name'};
    $f_name = $user->{'f_name'};
    $story = $user->{'story'};
    $email = $user->{'email'};
		$phone = preg_replace("/[^0-9]/", '', $user->{'phone'});
		
    if (strlen($phone) > 10) $phone = substr("$phone", -10);

    $save_user = $db->prepare("
			INSERT INTO $tables->users_info (id, name, s_name, f_name, phone, story) 
			VALUES (?, ?, ?, ?, ?, ?) 
			ON DUPLICATE KEY UPDATE name=?, s_name=?, f_name=?, phone=?, story=?
		");
		
    $save_user->bind_param('issssssssss', $id, $name , $s_name, $f_name, $phone, $story, $name , $s_name, $f_name, $phone, $story);
		$save_user->execute();
		
    if ($save_user) {
      $data['error'] = 0;
    } else $data['error'] = 2;
    $save_user->close();

    if ($user->{'secure'}) {
      if ($user->{'password'} == $user->{'d_password'}) {
        $password = md5($user->{'password'});
        $save_user = $db->prepare("UPDATE $tables->users SET password=?, email=? WHERE id=?");
        $save_user->bind_param("ssi", $password, $email, $id);
      }
    } else {
      $save_user = $db->prepare("UPDATE $t_users SET email=? WHERE id=?");
      $save_user->bind_param("si", $email, $id);
		}
		
    $save_user_result = $save_user->execute();
    $save_user->close();

	}

	header('HTTP/1.0 400 Bad Request');
	echo json_encode(array(
		'error' => 'Bad Request'
	));
}
?>