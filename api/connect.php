<?php
Class Wrap {
  static function request($data) {
    $mysqli = $data['mysqli'];
    $mysqli->close();
    return json_encode($data);
  }

  static function connect() {
    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
    $data = array();
    if ($mysqli->connect_errno) {
      $data['error'] = 1;
      $data['error_text'] = $mysqli->connect_errno;
    } else {
      $data['error'] = 0;
      $data['mysqli'] = $mysqli;
      $mysqli->set_charset("utf8");
    }
    return $data;
  }

  static function wrapper(Closure $fallback) {
    $result = self::connect();
    if($result['error'] != 0) {
      return $result;
    } else {
      $result_data = $fallback($result); // отложенное выполнение кода
      return self::request($result_data); 
    }
    return $data;
  }
}
?>